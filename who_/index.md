---
layout: default
title: Who is involved?
permalink: /who/
order: 40
redirect_from: /projects/
---

# Who is involved?

Various entities are working on providing reproducible builds to their users and developers:
{: .lead}

<br>

<div class="row">
<div class="col-md-4 text-center" markdown="1">

<a href="{{ "/who/people/" | relative_url }}"><img src="{{ "/images/who/people.svg" | relative_url }}" height="200"></a>

[People]({{ "/who/people/" | relative_url }})
{: .lead}

</div>
<div class="col-md-4 text-center" markdown="1">

<a href="{{ "/who/projects/" | relative_url }}"><img src="{{ "/images/who/projects.svg" | relative_url }}" height="200"></a>

[Projects]({{ "/who/projects/" | relative_url }})
{: .lead}

</div>
<div class="col-md-4 text-center" markdown="1">

<a href="{{ "/who/sponsors/" | relative_url }}"><img src="{{ "/images/who/sponsors.svg" | relative_url }}" height="200"></a>

[Sponsors]({{ "/who/sponsors/" | relative_url }})
{: .lead}

</div>
</div>

<br>
