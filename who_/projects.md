---
layout: default
title: Who is involved?
permalink: /who/projects/
---

# Involved projects

← *Back to [who is involved]({{ "/who/" | relative_url }})*.

<br>

Various free software projects are working on providing reproducible builds to their users and developers:
{: .lead}

<div class="projects row bg-light p-md-5 p-sm-3 pt-5 pb-5">
    {% for project in site.data.projects %}
    <div class="col-xs-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
        <div class="card" name="{{ project.name }}">
            <a href="{{ project.url }}" name="{{ project.name }}">
                <img class="p-5 project-img" src="{{ project.logo | prepend: "/images/logos/" | relative_url }}" alt="{{ project.name }}">
            </a>
            <div class="card-body">
                <h4 class="card-title"><a href="{{ project.url }}">{{ project.name }}</a></h4>
            </div>
            <ul class="list-group list-group-flush">
                {% for resource in project.resources %}
                    <li class="list-group-item">
                        <a href="{{ resource.url }}">{{ resource.name }}</a>
                    </li>
                {% endfor %}
                {% if project.tests %}
                    <li class="list-group-item">
                        <a href="{{ project.tests }}">Continuous tests</a>
                    </li>
                {% endif %}
            </ul>
        </div>
    </div>
    {% endfor %}
</div>

<br>

