---
layout: report
year: "2021"
month: "05"
title: "Reproducible Builds in May 2021"
draft: false
date: 2021-06-03 13:43:39
---

**Welcome to the May 2021 report from the [Reproducible Builds](https://reproducible-builds.org) project**
{: .lead}

[![]({{ "/images/reports/2021-05/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

In these reports we try to highlight the most important things that we have been up to over the past month. As a quick recap, whilst anyone may inspect the source code of free software for malicious flaws, almost all software is distributed to end users as pre-compiled binaries. If you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

---

[![]({{ "/images/reports/2021-05/executive-order.png#right" | relative_url }})](https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/)

The president of the United States [signed an executive order](https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/) this month outlining policies aimed to improve the cybersecurity in the US. The executive order comes after a number of highly-publicised security problems such as a ransomware attack that [affected an oil pipeline between Texas and New York](https://www.bbc.co.uk/news/business-57050690) and the [SolarWinds hack](https://www.theverge.com/2021/2/18/22288961/solarwinds-hack-100-companies-9-federal-agencies) that affected a large number of US federal agencies.

A [summary of the (8,000-word) document is available](https://www.whitehouse.gov/briefing-room/statements-releases/2021/05/12/fact-sheet-president-signs-executive-order-charting-new-course-to-improve-the-nations-cybersecurity-and-protect-federal-government-networks/), but section four is relevant in the context of reproducible builds. Titled "*Enhancing Software Supply Chain Security*", it outlines a plan that might involve:

> requiring developers to maintain greater visibility into their software and making security data publicly available. It stands up a concurrent public-private process to develop new and innovative approaches to secure software development and uses the power of Federal procurement to incentivize the market. Finally, it creates a pilot program to create an “energy star” type of label so the government – and the public at large – can quickly determine whether software was developed securely.

In response to this Executive Order, the US [National Institute of Standards and Technology](https://www.nist.gov/) (NIST) announced that [they would host a virtual workshop in early June](https://www.nist.gov/itl/executive-order-improving-nations-cybersecurity/workshop-and-call-position-papers) to both respond and attempt to fulfill its terms. In addition, David Wheeler published a blog post on the [Linux Foundation's blog](https://www.linuxfoundation.org/en/blog/) on the topic. Titled [*How LF communities enable security measures required by the US Executive Order on Cybersecurity*](https://www.linuxfoundation.org/en/blog/how-lf-communities-enable-security-measures-required-by-the-us-executive-order-on-cybersecurity/), David's post explicitly mentions reproducible builds, particularly the [Yocto Project](https://www.yoctoproject.org/)'s support for fully-reproducible builds.

<br>

[![]({{ "/images/reports/2021-05/QYH18NpsRu8.png#right" | relative_url }})](https://www.youtube.com/watch?v=QYH18NpsRu8)

David A. Wheeler [posted to our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2021-May/002248.html), to announce that the public defense of his [*Fully Countering Trusting Trust through Diverse Double-Compiliing (DDC)*](https://dwheeler.com/trusting-trust/) PhD thesis at [George Mason University](https://www2.gmu.edu/) is now available online.

<br>

[Dan Shearer announced a new tool](https://lists.reproducible-builds.org/pipermail/rb-general/2021-May/002269.html) called "[Not-Forking](https://lumosql.org/src/not-forking/doc/trunk/README.md) which attempts to avoid duplicating the source code of one project within another. This is highly relevant in the context of reproducible builds, as embedded code copies are often the cause of reproducibility: in many cases, addressing the problem upstream (and then ensuring a fixed version is available in distributions) is not a sufficient fix, as any embedded code copies remain unaffected. (This has been observed a number of times, particularly with embedded copies of `help2man` and similar documentation generation tools.)

<br>

[![]({{ "/images/reports/2021-05/archlinux.png#right" | relative_url }})](https://archlinux.org/)

Due to the [recent upheavals on the Freenode IRC network](https://en.wikipedia.org/wiki/Freenode#Ownership_change_and_conflict), the `#archlinux-reproducible` has moved to [Libera Chat](https://libera.chat/). (The more general `#reproducible-builds` IRC channel, which is hosted on the [OFTC](https://www.oftc.net/) network, has not moved.)

<br>

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general), Marcus Hoffman started a thread after finding that he was [unable to hunt down the cause of a unreproducible build of an Android APK package](https://lists.reproducible-builds.org/pipermail/rb-general/2021-May/002264.html) which Bernhard M. Wiedemann managed to track down to a '`pg-map-id`' field and a related checksum. This resulted in an [issue being reported against Google's Android toolchain](https://issuetracker.google.com/issues/189498001) which, as [Marcus himself wrote](https://lists.reproducible-builds.org/pipermail/rb-general/2021-May/002267.html), "hope it get's fixed this year".

<br>

[![]({{ "/images/reports/2021-05/debian.png#right" | relative_url }})](https://debian.org/)

Roland Clobus [reported on his progress towards making the Debian 'Live' image reproducible](https://lists.reproducible-builds.org/pipermail/rb-general/2021-May/002253.html) on [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general) this month, coordinating with Holger Levsen to add automatic, daily testing of Live images and producing [*diffoscope*](https://diffoscope.org) reports if not. Elsewhere in Debian, 9 reviews of Debian packages were added, 8 were updated and 29 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Chris Lamb also identified a new [`random_uuid_in_notebooks_generated_by_nbsphinx`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/c6a4aa47) toolchain issue.

## Software development

### Upstream patches

* Arthur Gautier created a pull request against the official [RubyGems](https://rubygems.org/) repository in order to cleanup temporary directories that affected reproducibility when building extensions.&nbsp;[[...](https://github.com/rubygems/rubygems/pull/4610)]

* Chris Lamb filed Debian bug [#988978](https://bugs.debian.org/988978) against [`apispec`](https://tracker.debian.org/pkg/apispec) ([forwarded upstream](https://github.com/marshmallow-code/apispec/pull/669)).

* Roland Clobus filed Debian bugs [#988976](https://bugs.debian.org/988976) and [#989203](https://bugs.debian.org/989203) against [`apt-cacher-ng`](https://tracker.debian.org/pkg/apt-cacher-ng) package.

* Paul Spooren [proposed a patch](http://lists.busybox.net/pipermail/busybox/2021-May/088842.html) for the [BusyBox](https://www.busybox.net/) suite of UNIX utilities popular on embedded systems so that it uses [`SOURCE_DATE_EPOCH`](https://reproducible-builds.org/specs/source-date-epoch/) for build timestamps if available. Debian and Arch Linux currently set `KCONFIG_NOTIMESTAMP=1` in their build systems to achieve the same, but as [Eli Schwartz added to the same thread](http://lists.busybox.net/pipermail/busybox/2021-May/088844.html) said: "for overall consistency and coherency, the spec on [reproducible-builds.org]({{ "/" | relative_url }}) does recommend every program respect one variable, `$SOURCE_DATE_EPOCH`, rather than needing to specify a different variable for each program".

* Bernhard M. Wiedemann:

    * [`coreutils`](https://bugzilla.opensuse.org/show_bug.cgi?id=1185520) (report build failure)
    * [`intltool`](https://code.launchpad.net/~invidian/intltool/fix-dangling-lock-file/+merge/400825) (Arch Linux developers improved my previous fix)
    * [`openssl-1_1`](https://bugzilla.opensuse.org/show_bug.cgi?id=1185637) (build failure)
    * [`openttd-opensfx`](https://build.opensuse.org/request/show/894257) (issue with `.tar`)
    * [`python-postorius`](https://bugzilla.opensuse.org/show_bug.cgi?id=1186156) (build gets stuck)
    * [`rehex`](https://github.com/solemnwarning/rehex/issues/129) (merged, report parallelism)
    * [`traefik`](https://build.opensuse.org/request/show/894441) + [`traefik1.7`](https://build.opensuse.org/request/show/895389) (date issue)

### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2021-05/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is the Reproducible Builds project in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it provides human-readable diffs from many kinds of binary formats. This month, [Chris Lamb](https://chris-lamb.co.uk) made a number of changes including releasing [version 174](https://diffoscope.org/news/diffoscope-174-released/), [version 175](https://diffoscope.org/news/diffoscope-175-released/) and [version 176](https://diffoscope.org/news/diffoscope-176-released/):

* Bug fixes:

    * Check that we are parsing an actual Debian `.buildinfo` file, not just a file with that particular extension — after all, it could be any file.&nbsp;([#254](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/254), [`#987994`](https://bugs.debian.org/987994))
    * Support signed `.buildinfo` files again. It appears that some versions of [`file(1)`](http://darwinsys.com/file/) reports them as `PGP signed message`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/698bb26)]
    * Use the actual filesystem path name (instead of *diffoscope*'s concept of the source archive name) in order to correct filename filtering when an APK file has been extracted from a container format. In particular, we need to filter the auto-incremented `1.apk` instead of `original-name.pk`.&nbsp;([#255](https://salsa.debian.org/reproducible-builds/diffoscope/issues/-255))

* New features:

    * Update `ffmpeg` tests to work with version 4.4.&nbsp;([#258](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/258))
    * Correct grammar in a `fsimage.py` debug message.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/fcb648e)]

* Misc:

    * Don't unnecessarily call `os.path.basename` twice in the Android APK comparator.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0ac914e)]
    * Added instructions on how to install *diffoscope* on [openSUSE](https://opensuse.org) on the *diffoscope* website [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/235a037)].
    * Add a comment about stripping filenames.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d223012)]
    * Corrected a reference to `site.salsa_url` which was breaking the "File a new issue" link on the website.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/4c2dc3e)]

In addition:

* Keith Smiley:

    * Improve support for Apple provisioning profiles.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ec3e353)]
    * Fix ignoring `objdump`-related tests on MacOS. MacOS has a version of `objdump(1)` that doesn't support `--info` so the tests would fail on that operating system.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/348c3af)]

* Mattia Rizzolo:

    * Fix recognition of compressed `.xz` archives with `file(1)` version 5.40.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/864644f)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2cd654b)]
    * Embed small test fixture in the code itself, rather than a separate file.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/df73767)]

### strip-nondeterminism

Chris Lamb made the following changes to [*strip-nondeterminism*](https://tracker.debian.org/pkg/strip-nondeterminism), our tool to remove specific non-deterministic results from a completed build:

* Added support for Python `pyzip` files: they require special handling to not mangle the UNIX shebang.&nbsp;([#18](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/-/issues/18))

* Dropped `single-debian-patch`, etc. from the Debian source package options.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/748e202)]

* Version `1.12.0-1` was [uploaded to Debian unstable](https://tracker.debian.org/news/1240523/accepted-strip-nondeterminism-1120-1-source-into-unstable/) by Chris Lamb.

### Website and documentation

[![]({{ "/images/reports/2021-05/website.png#right" | relative_url }})](https://reproducible-builds.org/)

Quite a few changes were made to the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) this month, including:

* Arnout Engelen:

    * Add a section regarding contributing to [NixOS](https://nixos.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/69c256b)]

* Chris Lamb:

    * Incorporate Holger Levsen's suggestion to improve the homepage text.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/ee935d8)]

* Holger Levsen:

    * Make the contribute page look a bit less like it is 'under construction', including explaining how we care about all distros and projects.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/03a3a85)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/8d68610)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/4f83bda)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/deddd80)]
    * Create an [Arch Linux contribution page]({{ "/contribute/archlinux/" | relative_url }}).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6349cf5)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/108a0f2)]
    * Make sponsor link visible in the sidebar.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/37913ed)]

* Ian Muchina:

    * Add syntax highlight styles.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/c110824)]

* Jelle van der Waa:

    * Add a few tasks to the [Arch Linux contribution page]({{ "/contribute/archlinux/" | relative_url }}) page.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6a4d684)]
    * Reflect that the `#archlinux-reproducible` IRC channel has moved to [Libera Chat](https://libera.chat/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/0131495)]

* Ludovic Courtès:

    * Explain how to contribute to reproducible builds related to [GNU Guix](https://guix.gnu.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3d14ec4)]

* Roland Clobus:

    * Added a trailing slash, fixing access to the Debian and Archlinux contribution pages.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/884ceef)]
    * Fix markup as reported by `msgfmt`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/0c32d7b)]


### Testing framework

[![]({{ "/images/reports/2021-05/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org). This month, the following changes were made:

* Holger Levsen:

    * Automatic node health check improvements:

        * Fix failed [`haveged`](http://www.issihosts.com/haveged/) entropy daemon services.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/523d4165)]
        * Remedy failed `user-runtime-dir@0` service invocations.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f800ed4d)]
        * Detect failures to connect to the [`review.coreboot.org`](https://review.coreboot.org/) [Gerrit](https://www.gerritcodereview.com/) code review instance.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b5ab03f9)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b4076493)]

    * Improvements to the `common-functions.sh` library:

        * Set a more sensible default for the locale early on.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/eb39d28f)]
        * Various visual improvements, including changes to script output.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2a199768)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e2ff6e2b)]
        * Improvement debug output.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/93dcb458)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d51dc8d4)]
        * Only notify an IRC channel if a channel is actually configured.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f98d6522)]

    * Improvements to cleanup routines:

        * Cleanup `sbuild(1)` directories using `sudo(8)` after three days.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5f5ca777)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bdc4ca89)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c6a25c95)]
        * Loosen a regular expression to detect failures when removing stuff.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fca3d447)]

    * Misc:

        * Increase kernel [`inotify(7)`](https://en.wikipedia.org/wiki/Inotify) watch limit further on all hosts. The value is now four times the default now.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f11e3a2f)]
        * Don't try to install the `devscripts` package from the *buster-backports* distribution.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/55855688)]
        * Improve grammar in some comments that are seen every day.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e4c00614)]

* Mattia Rizzolo:

    * Stop filtering out build failures due to `-ffile-prefix-map`: this flag is the default for the official `dpkg` package, so these are now "real" build failures.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/07652d29)]
    * Export package 'not for us' (NFU) and 'blacklist' states in the `reproducible.json` file, but keep excluding them from `tracker.json`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/37d9cc75)]
    * Update the IP addresses of `armhf` architecture hosts.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f62cc059)]
    * Properly alternate between `-amd64` and `-686` Debian kernels on the i386 architecture builders.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/44cae15f)]
    * Disable the `man-db` package everywhere, to save time in virtually all `apt` install/upgrade actions.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/76837355)]

* Vagrant Cascadian:

    * Add new `armhf` architecture build nodes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4921aaea)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3115fb2a)]
    * Retire all machines with only 2GiB of ram.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dbc29d39)]
    * Drop Debian *buster* kernel configurations for `cbxi4*` and `wbq0` hosts.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/622eabdd)]
    * Keep `imx6` systems running Debian *buster* kernels.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/27ec7ab4)]
    * Prepare to switch `armhf` nodes over to Debian *bullseye*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0984d498)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e5d4fff4)]

Finally, build node maintenance was performed by Holger Levsen&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c5b6a545)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/21e0b893)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/785c8dcc)] and Vagrant Cascadian&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6ad66746)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f2b71ecf)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/73a1b4de)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9771fd9a)]


<br>

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mastodon: [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)

<br>

This month's report was written by Chris Lamb, Holger Levsen and Vagrant Cascadian. It was subsequently reviewed by a bunch of Reproducible Builds folks on IRC and the mailing list.
{: .small}
