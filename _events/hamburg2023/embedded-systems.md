---
layout: event_detail
title: Collaborative Working Sessions - Embedded systems
event: hamburg2023
order: 202
permalink: /events/hamburg2023/embedded-systems/
---

# What are embedded systems:

* All systems which is not a PC, Software in a device.
* Industrial control (PLC = programmable logic control)
* Software in a toaster or dishwasher
* Small and powerful systems given:
* Small: TMS430 Series, 32 kByte ROM, 512 Byte RAM,
* Powerful: Raspi as controller
* => Focus to small systems, because powerful have often a Linux Kernel,
* Using a specific Real time operation system or not, no difference
* (If RTOS is used, it should be also reproducible)

# Challenges

* Different Target systems, different compilers from diff. vendors, more         variablility.
* comprehensibility process how the binary is built
* traceability of build process.
* Reprod. Build should be a part of the approval procedure for a device.

# Solutions:

* All solutions known for gcc should be also applied to all known compiler,
* Is it done or in focus? Responsibility from the compiler vendors for this topic

# Notes during session

* Reproducible required: Gambling industry requires it. restrict regulation.
* look for gcc link time optimization.

## Look for:

* Debugging information in the object:
* 1) with the same path, different machines.
* 2) diff. path on diff machines
* If the path is a symbolic link (or Junction in Windows), the Compiler should not resolve the symbolic linked path, then the path is able to make unique.
* Use Option remove debug symbols.
* Always use the same compiler version.


* Build info file is not very well standardized? Look for https://wiki.debian.org/ReproducibleBuilds/BuildinfoFiles
* Have a script for all stuff of build inputs
* Language for sorting its a topic to the build system. (for linker), use always US-ASCII-sorting
* Optimizations should not randomized or have start seed. SDE Source date epoch as seed.
* Parallel compilation makes problems? Order of files.

gcc is reproducible by default. with the same source path. Or have not debug symbols.
build-id.

## Changing and testing: Test effort. 

What about changing sources in a small way, reprod. build gives only small changes, test only the differences:

* Have to be more as one binary for the solutions. using dll libs ?
* Use static link, compare object files. Look from where comes the difference

