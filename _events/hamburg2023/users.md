---
layout: event_detail
title: Collaborative Working Sessions - Understanding user-facing needs and personas
event: hamburg2023
order: 31
permalink: /events/hamburg2023/users/
---

Clusters of stakeholders:
- 'end users' 
  - 'distro' end-users
  - 'direct' (non-distro) end-users
  - 'normies'
  - administrators
- organizations that want to use reproducible software
  - software vendors
  - (oss) developer communities
- intermediaries
  - distro/package managers
  - verifiers
  - managers/teamleaders

Goals:
- even developers are not aware of reproducible builds. Expected much less so to end-users, but already
- initiatives such as Debian mandating reproducibility

- example: f-droid built an apk with malware from package repository, while the original developer had a cached non-backdoored version.
- policy: most build pipelines nowadays have security compliance features, reproducibility might become a part of that. that helps developers care.
- even if source is available it can be hard to rebuild in practice.

- integration in package managers, so you can set a policy to only install reproducible software

- what about software does not found in distro packages
  - repro-env: makes it easier to rebuild 3rd-party packages
  - important that software is reproduced by people unaffiliated with the project

- Levels of trustworthiness:
  - low: source unknown, distributed by 'authority'
  - medium: open source
  - high: reproducible open source

- In case of F-Droid: there F-Droid takes the role of the 3rd party reproducing/verifying the software
  - extra advantage is that in case of F-Droid the APK built by F-Droid is compared
    to the APK built by the upstream.
    This is unfeasible for distro's, though, since distro's provide value by building
    packages in a particular way to provide a consistent experience to their users

- registry where independent 3rd party rebuilders/verifiers can upload their build results
  - in-toto plugin for arch and debian would be an interesting inspiration
  - how to organize/fund such rebuilders?
  - integrate rebuilding functionality into distro/package managers?
    - reproduce probabilistically?
  - some large organizations may want to rebuild for their own use anyway
    - if we make it easy for them, and entice them to share their results,
      the rest of the community could piggy-back on that?
    - rebuilderd? results queryable over http api


