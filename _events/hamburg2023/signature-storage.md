---
layout: event_detail
title: Collaborative Working Sessions - Signature storage and sharing
event: hamburg2023
order: 204
permalink: /events/hamburg2023/signature-storage/
---

* Most uses PGP keys, some uses SSH keys for commit signing (YubiKeys
  support HSM management of SSH keys)
* Key discovery is not always trivial
* Unclear story around how to verify signatures
* Commit signing can be hard as certain CI/CD systems either signes
  commits used in UI with their own key, or shows badges such as
  "commit verified". This only works of the CI/CD knows about all the
  commit sining keys, and so can show "commit not verified" which can
  be false or misleading
* For package manager, Maven contains each maintainer's public key
* Similar for many distributions (knows about maintainer's public
  keys)
* Android uses an allow list of developer keys
* In general, the security of allowed keys at resit is not resilient
  against tampering (i.e an attack on a server)
* TUF could be used to secure trusted keys (both at rest and in
  transit)
* Some pacakge repositories signs the packages (can still be signed by
  the developer before publish, i.e multiple signatures)
* With PGP, keys can be rotated. New key N+1 can be signed with
  current key N. Not possible with SSH keys
* Summary (for the general case):
    * Key distribution is hard
    * No easy verification flow



