---
layout: event_detail
title: Project updates - SUSE and openSUSE
event: hamburg2023
order: 20
permalink: /events/hamburg2023/suse-opensuse-updates/
---
## SUSE rb updates

[2022-06 announced SLSA level 4 for SLE-15-SP4](https://documentation.suse.com/sbp/server-linux/html/SBP-SLSA4/index.html), merged several upstream rb-patches into SLE (SUSE Linux Enterprise) codebase around that time.

some other employees joined in advancing rb

## openSUSE rb updates


Bernhard keeps analyzing issues, filing bug-reports, creating+submitting patches
around 700 since 2019-12


Around 97% of packages reproducible by now (500 of 15000 left; 130 with significant diffs after "build-compare" filter)


We fixed significant issues in python3.9 + 3.10, java/maven, ...


* remaining toolchain issues in:
    * [rpm](https://github.com/rpm-software-management/rpm/issues/2343)
    * [ghc](https://github.com/opensuse-haskell/ghc-rpm-macros/pull/1)
    * [mono](https://bugzilla.opensuse.org/show_bug.cgi?id=1141502)
    * [golang](https://github.com/golang/go/issues/63851)
    * [efl](https://git.enlightenment.org/enlightenment/efl/issues/41)
    * [libpinyin](https://github.com/libpinyin/libpinyin/issues/162)
    * sphinx
    * numba
    * ghostscript
    * pdflatex
