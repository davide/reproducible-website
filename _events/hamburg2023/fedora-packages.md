---
layout: event_detail
title: Collaborative Working Sessions - Fedora packages
event: hamburg2023
order: 306
permalink: /events/hamburg2023/fedora-packages/
---

Bernhard imported a number of packages into openSUSE's open-build-service(OBS) and used the reproducibleopensuse tools to do double-builds and verification builds and additionally compare to official Fedora binary pkgs

Fedora does not normalize build-time in rpm-headers, so no bit-reproducible replication is possible atm. Would need some rpm %macro mechanism to override the value that is separate from $SOURCE_DATE_EPOCH.

Additionally we found some more roadblocks:
	* Fedora uses dynamic python provides that ended up missing in OBS and resulted in a failed build
	* pam did not build because xpdf xpdf-libs both provided the same symbol and OBS does not have Fedoras's automatic resolution of using the first shortest name
	* python .pyc file headers varied - maybe because of additional rb-related macros used
	* koji only kept details from the last 2 weeks, so buildroot details for all Fedora39 packages were already expired from that cache (though are accessible in a different way)
	* OBS has a different logic to create Release strings (N.M vs N.fc39)
	* The name-epoch-version-release string gets embedded in `.note.package` section in all ELF files, which means it'll directly impact reproducibility.

we tested with
	* 2ping
	* perl-Alien-Brotli
	* python-gemfileparser2
	* pam

