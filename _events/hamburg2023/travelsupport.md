---
layout: default
title: Hamburg 2023 - Travel Bursary
permalink: /events/hamburg2023/travelsupport
event_hide: true
event_date: 2023-10-30
event_date_string: October 30th, November 1st-2nd 2023
event_location: Hamburg, Germany
---

# {{ page.title }}

**<a href="{{ "/events/hamburg2023" | relative_url }}">← Main event page</a>**

### Instructions for requesting travel and/or accommodation sponsorship

If you are seeking sponsorship to attend this event, please email the organizers with these details:

    I'm seeking sponsorship for:
    [ ] accommodation
    [ ] travel
        * I'm travelling from ____ via _____ (main mode of transportation)
        * When going back I'll go to _____ via ____ (main mode of transportation)
        * I foresee the expense will be ______ €

You will need to pay the travel from your own pocket and you will be reimbursed *after* the event; in case you won't be able to attend the event we won't be providing reimbursement.

You will receive confirmation of the bursary by October 1st 2023.
The budget for sponsored attendance is limited, so please do send your request early.


If you'd rather not have such information recorded on a mailing list, please send the email directly to Mattia instead.
