---
layout: post
title: "Second Reproducible Builds IRC meeting"
date: 2020-10-26 08:32:00
draft: false
---

[![]({{ "/images/news/reproducible-irc-meeting-2/irc.png#right" | relative_url }})]({{ "https://time.is/compare/1800_12_Oct_2020_in_UTC" | relative_url }})

After the success of our [previous IRC meeting]({{ "/news/2020/10/12/reproducible-irc-meeting/" | relative_url }}), we are having our second IRC meeting today. Monday 26th October, at [**18:00 UTC**](https://time.is/compare/1800_26_Oct_2020_in_UTC):

* 11:00am San Francisco. [[...](https://time.is/compare/1800_26_Oct_2020_in_UTC/San_Francisco)]
* 2:00pm New York. [[...](https://time.is/compare/1800_26_Oct_2020_in_UTC/New_York)]
* 6:00pm London. [[...](https://time.is/compare/1800_26_Oct_2020_in_UTC/London)]
* 7:00pm Paris/Berlin. [[...](https://time.is/compare/1800_26_Oct_2020_in_UTC/Berlin)]
* 11:30pm Delhi. [[...](https://time.is/compare/1800_26_Oct_2020_in_UTC/Delhi)]
* 2:00am Beijing. [[...](https://time.is/compare/1800_26_Oct_2020_in_UTC/Beijing)] (+1 day)

Please join us on the `#reproducible-builds` channel on [irc.oftc.net](https://oftc.net/) — an [agenda is available](https://pad.sfconservancy.org/p/reproducible-builds-meeting-agenda). As mentioned in our previous meeting announcement, due to the unprecedented events in 2020, there will be [no in-person Reproducible Builds event this year](https://lists.reproducible-builds.org/pipermail/rb-general/2020-September/002045.html), but we plan to run these IRC meetings every fortnight.
