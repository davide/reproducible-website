---
layout: default
title: Events
permalink: /events/
order: 60
---

# Events

Events are organized to exchange ideas about reproducible builds, get a better
understanding and cooperate, be it on improving code or specifications or something new, which noone saw coming in retrospect.
<br/>
(Due to the unprecedented events of 2020 and following, there were no in-person events during two years.)

{% assign sorted_events = site.events | sort: 'event_date' | reverse %}
{% for page in sorted_events %}
{% if page.event_date_string and page.event_hide != true %}
## {{ page.title }}

*{{ page.event_date_string }}* — {{ page.event_location }}

{{ page.event_summary }}

[Read more…]({{ page.permalink | relative_url }})
{% endif %}
{% endfor %}
